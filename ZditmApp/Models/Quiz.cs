﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ZditmApp.Models
{
    public class Quiz
    {
        private List<QuizQuestion> questions;
        private int questionLimit;

        public Quiz(int limit)
        {
            questionLimit = limit;
            questions = new List<QuizQuestion>();
            PrepareQuiz();
        }

        public List<QuizQuestion> Questions
        {
            get
            {
                return questions;
            }
        }

        private void PrepareQuiz()
        {
            List<QuizQuestion> selectedQuestions = new List<QuizQuestion>();
            ApplicationDbContext context = new ApplicationDbContext();
            List<QuizQuestion> questionList = context.Database.SqlQuery<QuizQuestion>("SELECT * FROM AspNetQuestions").ToList();
            selectedQuestions.AddRange(questionList.Where(m => m.LastUsed == null).Take(questionLimit));
            List<QuizQuestion> previouslyUsedQuestions = questionList.Where(m => m.LastUsed != null).OrderBy(m => m.LastUsed).ToList();
            foreach (var question in selectedQuestions)
            {
                question.LastUsed = DateTime.Now;
                context.Database.ExecuteSqlCommand("UPDATE AspNetQuestions SET lastUsed='" + question.LastUsed.ToString() + "' WHERE Id='" + question.Id.ToString() + "'");
            }
            int currentQuestionCounter = 0;
            while (selectedQuestions.Count < questionList.Count && selectedQuestions.Count < questionLimit)
            {
                var currentQuestion = previouslyUsedQuestions.ElementAt(currentQuestionCounter);
                currentQuestion.LastUsed = DateTime.Now;
                context.Database.ExecuteSqlCommand("UPDATE AspNetQuestions SET lastUsed='" + currentQuestion.LastUsed.ToString() + "' WHERE Id='" + currentQuestion.Id.ToString() + "'");
                selectedQuestions.Add(currentQuestion);
                currentQuestionCounter++;
            }
            questions = selectedQuestions;
        }
    }
}