﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZditmApp.Models
{
    public class QuizQuestion
    {
        private int id;
        private string content;
        private string answer;
        private DateTime? lastUsed;

        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Content
        {
            get
            {
                return content;
            }
            set
            {
                content = value;
            }
        }

        public string Answer
        {
            get
            {
                return answer;
            }
            set
            {
                answer = value;
            }
        }

        public DateTime? LastUsed
        {
            get
            {
                return lastUsed;
            }
            set
            {
                lastUsed = value;
            }
        }        
    }
}