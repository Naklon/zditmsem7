﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace ZditmApp.Models
{
    public class AppUser
    {
        public int Id;
        private string email;
        public bool EmailConfirmed;
        public string PasswordHash;
        public string SecurityStamp;
        public string PhoneNumber;
        public bool PhoneNumberConfirmed;
        public bool TwoFactorEnabled;
        public DateTime LockoutEndDateUtc;
        public bool LockoutEnabled;
        public int AccessFailedCount;
        public string userName;
        private int? pointsMonth;
        private int? pointsTotal;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                email = value;
            }
        }
        public int? PointsTotal
        {
            get
            {
                return pointsTotal;
            }
            set
            {
                pointsTotal = value;
            }
        }
            public int? PointsMonth
        {
            get
            {
                return pointsMonth;
            }
            set
            {
                pointsMonth = value;
            }
        }

    }
}