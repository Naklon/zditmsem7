﻿using CrossPlatformLibrary.Geolocation;
using GeoWatcher;
using Microsoft.AspNet.Identity;
using Microsoft.Practices.ServiceLocation;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZditmApp.Models;

namespace ZditmApp.Controllers
{
    public class HomeController : Controller
    {

        public ActionResult Index()
        {            
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Gifts()
        {
            return View();
        }

        [Authorize]
        public ActionResult Menu()
        {
            return View();
        }


        [Authorize]
        public ActionResult TakeQuiz()
        {
            if (AppData.CURRENT_QUIZ != null)
            {
                ApplicationDbContext context = new ApplicationDbContext();
                DateTime lastTaken = context.Database.SqlQuery<DateTime>("SELECT LastTaken FROM dbo.AspNetUsersQuizes WHERE UserId = @UserId", 
                    new SqlParameter("@UserId", User.Identity.GetUserId())).SingleOrDefault();
                context.Dispose();
                if (DateTime.Now.Subtract(lastTaken).Days >= 3)
                {
                    return View(AppData.CURRENT_QUIZ);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [Authorize]
        public ActionResult Ranking()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            ViewBag.Users = context.Database.SqlQuery<AppUser>("SELECT * FROM AspNetUsers").OrderByDescending(m=>m.PointsTotal).ToList();
            context.Dispose();
            return View();
        }

        [HttpPost]
        public ActionResult TakeQuiz(params string[] answers)
        {
            bool ifCorrect = true;
            int questionNumber = 0;
            foreach (var answer in answers)
            {
                if (!answer.ToLower().Contains(AppData.CURRENT_QUIZ.Questions.ElementAt(questionNumber).Answer.ToLower()))
                {
                    ifCorrect = false;
                    break;
                }
                questionNumber++;
            }

            if(ifCorrect)
            {
                AppData.USER_MANAGER.AddUserPoints(User.Identity.GetUserId(), 10);
            }
            ApplicationDbContext context = new ApplicationDbContext();
            context.Database.ExecuteSqlCommand("DELETE FROM AspNetUsersQuizes WHERE UserId = @UserId",
               new SqlParameter("@UserId", User.Identity.GetUserId()));
            context.Database.ExecuteSqlCommand("INSERT INTO AspNetUsersQuizes VALUES (@UserId, @LastTaken)",
                new SqlParameter("@UserId", User.Identity.GetUserId()), new SqlParameter("@LastTaken", DateTime.Now));
            context.Dispose();
            return RedirectToAction("Index");
        }


        public ActionResult Contact()
        {
            return View();
        }
    }
}