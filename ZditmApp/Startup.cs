﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ZditmApp.Startup))]
namespace ZditmApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
