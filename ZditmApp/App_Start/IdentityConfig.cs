﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using ZditmApp.Models;
using System.Data.SqlClient;

namespace ZditmApp
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Dołącz tutaj usługę poczty e-mail, aby wysłać wiadomość e-mail.
            return Task.FromResult(0);
        }
    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Dołącz tutaj usługę wiadomości SMS, aby wysłać wiadomość SMS.
            return Task.FromResult(0);
        }
    }

    // Skonfiguruj menedżera użytkowników aplikacji używanego w tej aplikacji. Interfejs UserManager jest zdefiniowany w produkcie ASP.NET Identity i jest używany przez aplikację.
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(IUserStore<ApplicationUser> store)
            : base(store)
        {
        }

        public int GetUserPointsMonth(string userId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            int points = context.Database.SqlQuery<int>("SELECT PointsMonth FROM AspNetUsers WHERE Id='" + userId + "'").Single();
            context.Dispose();
            return points;
        }

        public int GetUserPointsTotal(string userId)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            int points = context.Database.SqlQuery<int>("SELECT PointsTotal FROM AspNetUsers WHERE Id='" + userId + "'").Single();
            context.Dispose();
            return points;
        }

        public void AddUserPoints(string userId, int points)
        {
            int currentPointsMonth = GetUserPointsMonth(userId);
            int currentPointsTotal = GetUserPointsTotal(userId);
            currentPointsMonth += points;
            currentPointsTotal += points;
            SetUserPointsMonth(userId, currentPointsMonth);
            SetUserPointsTotal(userId, currentPointsTotal);
        }

        public void SetUserPointsMonth(string userId, int points)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.Database.ExecuteSqlCommand("UPDATE AspNetUsers SET PointsMonth='" + points + "' WHERE Id='" + userId + "'");
            context.Dispose();
        }

        public void SetUserPointsTotal(string userId, int points)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.Database.ExecuteSqlCommand("UPDATE AspNetUsers SET PointsTotal='" + points + "' WHERE Id='" + userId + "'");

            context.Dispose();
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(new UserStore<ApplicationUser>(context.Get<ApplicationDbContext>()));
            // Konfiguruj logikę weryfikacji nazw użytkowników
            manager.UserValidator = new UserValidator<ApplicationUser>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Konfiguruj logikę weryfikacji haseł
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = true,
                RequireDigit = true,
                RequireLowercase = true,
                RequireUppercase = true,
            };

            // Konfiguruj ustawienia domyślne blokady użytkownika
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Zarejestruj dostawców uwierzytelniania dwuetapowego. W przypadku tej aplikacji kod weryfikujący użytkownika jest uzyskiwany przez telefon i pocztą e-mail
            // Możesz zapisać własnego dostawcę i dołączyć go tutaj.
            manager.RegisterTwoFactorProvider("Kod — telefon", new PhoneNumberTokenProvider<ApplicationUser>
            {
                MessageFormat = "Twój kod zabezpieczający: {0}"
            });
            manager.RegisterTwoFactorProvider("Kod — e-mail", new EmailTokenProvider<ApplicationUser>
            {
                Subject = "Kod zabezpieczeń",
                BodyFormat = "Twój kod zabezpieczający: {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }

    // Skonfiguruj menedżera logowania aplikacji używanego w tej aplikacji.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, string>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
}
